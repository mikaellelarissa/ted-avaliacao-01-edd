package application;

import set.ClienteSet;
import java.util.List;

public class Cliente implements ClienteSet {

	String nome;
	String telefone;
	String email;
	
	public Cliente() {
		
	}
	
	public Cliente(String nome, String telefone, String email) {
		this.nome = nome;
		this.telefone = telefone;
		this.email = email;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public boolean contains(List<Cliente> cliente, Cliente cliente1) {
		boolean consultar;
		consultar = cliente.contains(cliente1);
		return consultar;
	}

	@Override
	public boolean add(List<Cliente> cliente, Cliente cliente1) {
		boolean adicionar;
		adicionar = cliente.add(cliente1);
		return adicionar;
	}

	@Override
	public boolean remove(List<Cliente> cliente, Cliente cliente1) {
		boolean remover;
		remover = cliente.remove(cliente1);
		return remover;
	}

	@Override
	public int size(List<Cliente> cliente) {
		return cliente.size();
	}

	@Override
	public void clear(List<Cliente> cliente) {
		cliente.clear();
	}

}
